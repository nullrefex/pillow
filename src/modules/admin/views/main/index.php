<?php
/** @var $this \yii\web\View */

use yii\helpers\Url;

$this->title = Yii::t('app', 'Dashboard');
?>
<div class="admin-index">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Yii::t('app', 'Dashboard') ?></h1>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-cogs fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div>Кеш</div>
                </div>
            </div>
        </div>
        <a href="<?= Url::to(['/admin/tools/flush-cache', 'redirect_url' => Url::current()]) ?>"
           data-confirm="Вы уверены?">
            <div class="panel-footer">
                <span class="pull-left"><i class="fa fa-trash"></i> Очистить</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>

<!--
<div class="col-lg-3 col-md-6">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-picture-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div>Кеш изображений</div>
                </div>
            </div>
        </div>
        <a href="<?= Url::to(['/admin/tools/flush-thumbs', 'redirect_url' => Url::current()]) ?>"
           data-confirm="Вы уверены?">
            <div class="panel-footer">
                <span class="pull-left"><i class="fa fa-trash"></i> Очистить</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
-->
<div class="col-lg-3 col-md-6">
    <div class="panel panel-green">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-database fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div>Дамп БД</div>
                </div>
            </div>
        </div>
        <a href="<?= Url::to(['/admin/tools/db-dump']) ?>">
            <div class="panel-footer">
                <span class="pull-left"><i class="fa fa-download"></i> Скачать</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>