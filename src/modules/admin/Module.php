<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\admin;


use nullref\core\interfaces\IHasMigrateNamespace;
use nullref\fulladmin\Module as BaseModule;

class Module extends BaseModule implements IHasMigrateNamespace
{
    /**
     * @param $defaults
     * @return mixed
     */
    public function getMigrationNamespaces($defaults)
    {
        return ['nullref\fulladmin\migrations'];
    }

    public function init()
    {
        parent::init();
        if (!YII_CLI) {
            $this->controllerNamespace = '\app\modules\admin\controllers';
        }
    }
}