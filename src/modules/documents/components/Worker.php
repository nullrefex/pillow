<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\components;


use app\modules\documents\models\Document;
use yii\base\Component;

abstract class Worker extends Component
{
    const OPTION_TYPE_TEXT = 'text';
    const OPTION_TYPE_DROPDOWN = 'dropdown';
    const OPTION_TYPE_CHECKBOX = 'checkbox';
    const OPTION_TYPE_CHECKBOXES = 'checkboxes';
    const OPTION_TYPE_WIDGET = 'widget';

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @return array
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return [];
    }


    /**
     * @return array
     */
    public function getDocumentOptions()
    {
        return [];
    }

    /**
     * @param $document Document
     * @param $key
     * @return mixed
     */
    public function getOptionValue($document, $key)
    {
        return $document->getOptionValue($key);
    }
}