<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\components;


use app\modules\documents\models\Document;

abstract class BaseImporter extends Worker
{
    /**
     * @param Document $document
     * @return mixed
     */
    abstract public function import(Document $document);
}