<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2018 NRE
 */


namespace app\modules\documents\components;


abstract class YmlImporter extends BaseImporter
{
    public function getDocumentOptions()
    {
        return parent::getDocumentOptions();
    }

    protected function loadYmlFile($document)
    {
        $filePath = $this->getOptionValue($document, 'file_path');
        return simplexml_load_file($filePath);
    }

}