<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\models\config;


use app\modules\documents\components\BaseExporter;
use app\modules\documents\helpers\Helper;
use app\modules\documents\models\DocumentConfig;
use app\modules\documents\traits\Export as ExportTrait;

class Export extends DocumentConfig
{
    use ExportTrait;

    /**
     * @var null|BaseExporter
     */
    protected $_exporter = null;

    /**
     * @return array
     */
    public function getColumnsFormData()
    {
        return array_merge($this->getColumnsFormTemplate(), $this->getColumnsAssoc());
    }

    /**
     * @return array
     */
    public function getColumnsFormTemplate()
    {
        $columns = [];
        foreach ($this->getExporter()->getColumns() as $key => $label) {
            $columns[$key] = [
                'label' => $label,
                'target' => $key,
                'filter' => false,
                'source' => '',
            ];
        }
        return $columns;
    }

    /**
     * @return BaseExporter
     */
    public function getExporter()
    {
        if ($this->_exporter === null) {
            $this->_exporter = Helper::createExporter($this->options['exporter']);
        }
        return $this->_exporter;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['columns', 'options'], 'safe'],
            [['type'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    protected function getColumnsAssoc()
    {
        $columns = [];
        foreach ($this->columns ?: [] as $column) {
            $columns[$column['target']] = $column;
        }
        return $columns;
    }
}