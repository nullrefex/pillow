<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\models\document;


use app\modules\documents\models\config\Import as ImportConfig;
use app\modules\documents\models\Document;
use app\modules\documents\traits\Import as ImportTrait;

class Import extends Document
{
    use ImportTrait;

    public function afterDelete()
    {
        if (file_exists($this->file_path)) {

        }
        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfig()
    {
        return $this->hasOne(ImportConfig::className(), ['id' => 'config_id']);
    }

}