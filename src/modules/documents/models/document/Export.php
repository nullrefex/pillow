<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\models\document;


use app\modules\documents\models\config\Export as ExportConfig;
use app\modules\documents\models\Document;
use app\modules\documents\traits\Export as ExportTrait;

class Export extends Document
{
    use ExportTrait;

    public function afterDelete()
    {
        if (file_exists($this->file_path)) {

        }
        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfig()
    {
        return $this->hasOne(ExportConfig::className(), ['id' => 'config_id']);
    }
}