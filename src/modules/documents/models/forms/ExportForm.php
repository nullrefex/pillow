<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\modules\documents\models\forms;


use app\modules\documents\helpers\Helper;
use app\modules\documents\jobs\Export as ExportJob;
use app\modules\documents\models\config\Export as ExportConfig;
use app\modules\documents\models\document\Export;
use Yii;
use yii\base\Model;

class ExportForm extends Model
{
    public $file;
    public $configId;
    public $options;

    /** @var ExportConfig|null */
    protected $_config;

    /**
     * @param Export $model
     * @param bool $rerun
     */
    public static function runJob($model, $rerun = false)
    {
        $job = new ExportJob();
        $job->id = $model->id;
        $job->rerun = $rerun;
        $model->status = Export::STATUS_NEW;
        $model->save();
        Helper::getModule()->addJob($job);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['configId', 'required'],
            [['options'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function createRecord()
    {
        $model = Export::create();
        $model->type = $this->getDocumentConfig()->type;
        $model->config_id = $this->getDocumentConfig()->id;
        $model->options = $this->options;

        if ($model->save()) {
            self::runJob($model);
        } else {
            return false;
        }

        return true;
    }

    /**
     * @return ExportConfig|null
     */
    public function getDocumentConfig()
    {
        if ($this->_config === null) {
            $this->_config = ExportConfig::findOne($this->configId);
        }
        return $this->_config;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'configId' => Yii::t('documents', 'Config'),
            'file' => Yii::t('documents', 'File'),
            'options' => Yii::t('documents', 'Options'),
        ];
    }

}