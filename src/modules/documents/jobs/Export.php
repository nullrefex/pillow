<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\jobs;


use app\modules\documents\models\document\Export as ExportDocument;
use yii\helpers\Console;
use zhuravljov\yii\queue\Job;
use zhuravljov\yii\queue\Queue;

class Export implements Job
{
    public $id;

    public $rerun;

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        $document = ExportDocument::findOne($this->id);
        Console::output('Load document ID:' . $this->id);

        /** @var \app\modules\documents\models\config\Export $config */
        $config = $document->config;
        Console::output('Get config');
        $exporter = $config->getExporter();
        Console::output('Create exporter: ' . get_class($exporter));
        Console::output('Export started');
        try {
            $exporter->export($document);
        } catch (\Exception $e) {
            Console::output($e->getMessage());
        }
    }
}