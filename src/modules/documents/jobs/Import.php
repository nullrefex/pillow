<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\jobs;


use app\modules\documents\models\document\Import as ImportDocument;
use yii\helpers\Console;
use zhuravljov\yii\queue\Job;
use zhuravljov\yii\queue\Queue;

class Import implements Job
{
    public $id;

    public $rerun = false;

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        $document = ImportDocument::findOne($this->id);
        Console::output('Load document ID:' . $this->id);

        /** @var \app\modules\documents\models\config\Import $config */
        $config = $document->config;
        Console::output('Get config');
        $importer = $config->getImporter();
        Console::output('Create importer: ' . get_class($importer));
        Console::output('Import started');
        $document->options = array_merge($document->options, ['rerun' => $this->rerun]);
        $importer->import($document);
    }
}