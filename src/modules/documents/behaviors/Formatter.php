<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\behaviors;


use app\modules\documents\models\DocumentItem;
use Yii;
use yii\base\Behavior;

class Formatter extends Behavior
{
    /**
     * @param $status
     * @return string
     */
    public function asDocumentItemStatus($status)
    {
        $statuses = DocumentItem::getStatuses();
        if (isset($statuses[$status])) {
            return $statuses[$status];
        }
        return Yii::t('documents', 'N\A');
    }

}