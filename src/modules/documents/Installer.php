<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace app\modules\documents;


use nullref\core\components\ModuleInstaller;
use Yii;

class Installer extends ModuleInstaller
{
    public function getModuleId()
    {
        return 'documents';
    }

} 