<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\documents\traits;


use app\modules\documents\models\Document;
use yii\db\ActiveQueryInterface;

/**
 * Trait Import
 * @package app\modules\documents\traits
 *
 * @property $type
 */
trait Import
{
    /**
     * @return ActiveQueryInterface
     */
    public static function find()
    {
        return parent::find()->andWhere(['type' => Document::TYPE_IMPORT]);
    }

    /**
     * @return Document
     */
    public static function create()
    {
        $object = new self();
        $object->type = Document::TYPE_IMPORT;
        return $object;
    }
}