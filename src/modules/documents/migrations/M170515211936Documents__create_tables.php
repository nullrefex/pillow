<?php

namespace app\modules\documents\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170515211936Documents__create_tables extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->execute(<<<SQL
INSERT INTO `document_config` (`id`, `name`, `columns`, `created_at`, `updated_at`, `type`, `options`)
VALUES
	(1, 'Імпорт каталогу', '{\"vendor\":{\"target\":\"vendor\",\"label\":\"Vendor\",\"source\":\"1\",\"filter\":\"1\"},\"sku\":{\"target\":\"sku\",\"label\":\"Sku\",\"source\":\"2\",\"filter\":\"1\"},\"color\":{\"target\":\"color\",\"label\":\"Color\",\"source\":\"4\",\"filter\":\"1\"},\"size\":{\"target\":\"size\",\"label\":\"Size\",\"source\":\"5\",\"filter\":\"1\"},\"qty\":{\"target\":\"qty\",\"label\":\"Qty\",\"source\":\"6\",\"filter\":\"1\"},\"name\":{\"target\":\"name\",\"label\":\"Name\",\"source\":\"7\",\"filter\":\"1\"},\"material\":{\"target\":\"material\",\"label\":\"Material\",\"source\":\"8\",\"filter\":\"1\"},\"vendor_country\":{\"target\":\"vendor_country\",\"label\":\"Vendor country\",\"source\":\"9\",\"filter\":\"1\"},\"description\":{\"target\":\"description\",\"label\":\"Description\",\"source\":\"10\",\"filter\":\"1\"},\"season\":{\"target\":\"season\",\"label\":\"Season\",\"source\":\"11\",\"filter\":\"1\"},\"style\":{\"target\":\"style\",\"label\":\"Style\",\"source\":\"12\",\"filter\":\"1\"},\"base_price\":{\"target\":\"base_price\",\"label\":\"Base Price\",\"source\":\"13\",\"filter\":\"1\"},\"price\":{\"target\":\"price\",\"label\":\"Price\",\"source\":\"14\",\"filter\":\"1\"}}', 1494928802, 1499562858, 1, '{\"importer\":\"catalog\",\"first_row\":\"2\",\"delimiter\":\";\",\"create_if_not_exist\":\"1\"}');

SQL
        );
    }

    public function down()
    {
        $this->delete('{{%document}}');
        $this->delete('{{%document_config}}');
    }
}
