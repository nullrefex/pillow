<?php

namespace app\modules\settings\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170515211935Settings__create_table extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->createTable('{{%settings}}',
            [
                'id' => $this->primaryKey(),
                'type' => $this->string(255)->notNull(),
                'section' => $this->string(255)->notNull(),
                'key' => $this->string(255)->notNull(),
                'value' => $this->text(),
                'active' => $this->boolean(),
                'created' => $this->dateTime(),
                'modified' => $this->dateTime(),
            ]
        );
        $this->createIndex('settings_unique_key_section', '{{%settings}}', ['section', 'key'], true);

    }

    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}
