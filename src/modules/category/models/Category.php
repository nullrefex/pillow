<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\category\models;


use app\modules\eav\models\Attribute;
use app\modules\eav\models\attribute\Option;
use app\modules\product\models\Product;
use app\modules\product\models\service\CategoryBinder;
use nullref\category\models\Category as BaseCategory;
use nullref\useful\behaviors\JsonBehavior;

/**
 * Class Category
 * @package app\modules\category\models
 *
 * @property string $prom_id
 * @property string $product_bind
 */
class Category extends BaseCategory
{
    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['prom_id', 'unique'],
            [['product_bind'], 'safe'],
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonBehavior::class,
                'fields' => [
                    'product_bind',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getProductBindFields()
    {
        return [
            'name' => 'Назва',
            'material' => 'Склад (матеріал)',
            'size' => 'Розмір',
            'sku' => 'SKU (Актикул)',
        ];
    }

    public function getBindConditions()
    {
        if (!is_array($this->product_bind)) {
            return [];
        }
        return $this->product_bind;
    }

    public static function findProducts($conditions, CategoryBinder $model)
    {
        $query = Product::find();

        if (empty($conditions)) {
            return [];
        }

        $preparedConditions = [];

        foreach ($conditions as $item) {
            $field = $item['field'];
            $value = $item['value'];
            if (isset($preparedConditions[$field])) {
                if (is_array($preparedConditions[$field])) {
                    $preparedConditions[$field][] = $value;
                } else {
                    $preparedConditions[$field] = [$preparedConditions[$field], $value];
                }
            } else {
                $preparedConditions[$field] = $value;
            }
        }

        foreach ($preparedConditions as $field => $value) {
            switch ($field) {
                case 'name':
                    if (is_array($value)) {
                        $condition = ['or'];
                        foreach ($value as $subvalue) {
                            $condition[] = ['like', 'name', $subvalue];
                        }
                        $query->andWhere($condition);
                    } else {
                        $query->andWhere(['like', 'name', $value]);
                    }
                    break;
                case 'sku':
                    /** @var Attribute $attribute */
                    $attribute = Attribute::findOne(['code' => 'sku']);
                    $valueModel = $attribute->createValue();
                    $valueModel->load(['value' => $value], '');
                    $valueModel->addJoin($query, Product::tableName());
                    $valueModel->addWhere($query);
                    break;
                case 'material':
                    /** @var Attribute $attribute */
                    $attribute = Attribute::findOne(['code' => 'material']);
                    $valueModel = $attribute->createValue();
                    $valueModel->load(['value' => $value], '');
                    $valueModel->addJoin($query, Product::tableName());
                    $valueModel->addWhere($query);
                    break;
                case 'size':
                    $options = Option::find()->andWhere(['value' => $value])->all();
                    if (count($options)) {
                        if (count($options) > 1) {
                            /** @var Attribute $attribute */
                            $attribute = Attribute::findOne(['code' => 'size']);
                            $valueModel = $attribute->createValue();
                            $valueModel->load(['value' => array_map(function ($option) {
                                return $option->id;
                            }, $options)], '');

                            $valueModel->addJoin($query, Product::tableName());
                            $valueModel->addWhere($query);
                        } else {
                            /** @var Attribute $attribute */
                            $attribute = Attribute::findOne(['code' => 'size']);
                            $valueModel = $attribute->createValue();
                            $valueModel->load(['value' => $options[0]->id], '');

                            $valueModel->addJoin($query, Product::tableName());
                            $valueModel->addWhere($query);
                        }
                    }

                    break;
            }
        }
        if ($model->debug) {
            $model->log($query->createCommand()->getRawSql());
        }
        return $query->asArray()->all();
    }
}