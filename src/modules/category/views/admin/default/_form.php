<?php

use app\modules\category\models\Category;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model nullref\category\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'prom_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'parent_id')->hiddenInput()->label(false) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'product_bind')->widget(MultipleInput::className(), [
                'allowEmptyList' => true,
                'columns' => [
                    [
                        'name' => 'field',
                        'title' => 'Поле',
                        'options' => [
                            'readonly' => true,
                        ],
                        'type' => MultipleInputColumn::TYPE_DROPDOWN,
                        'items' => Category::getProductBindFields(),
                    ],
                    [
                        'name' => 'value',
                        'title' => 'Значення',
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'defaultValue' => '',
                    ],
                ],
            ])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('category', 'Create') : Yii::t('category', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
