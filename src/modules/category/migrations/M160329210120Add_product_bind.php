<?php

namespace app\modules\category\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M160329210120Add_product_bind extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if ($this->tableExist('{{%category}}')) {
            $this->addColumn('{{%category}}', 'product_bind', $this->text());
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->tableExist('{{%category}}')) {
            $this->dropColumn('{{%category}}', 'product_bind');
        }
        return true;
    }
}
