<?php

namespace app\modules\category\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M160329210119Add_prom_id extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if ($this->tableExist('{{%category}}')) {
            $this->addColumn('{{%category}}', 'prom_id', $this->string());
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->tableExist('{{%category}}')) {
            $this->dropColumn('{{%category}}', 'prom_id');
        }
        return true;
    }
}
