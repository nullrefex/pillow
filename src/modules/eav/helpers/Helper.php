<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\eav\helpers;


use app\modules\eav\Module;
use Yii;

class Helper
{
    /**
     * @return null|\yii\base\Module|\app\modules\eav\Module
     */
    public static function getModule()
    {
        return Yii::$app->getModule(Module::MODULE_ID);
    }
}