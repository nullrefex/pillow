<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\storage\traits;


use app\modules\storage\models\StorageHasItem;
use yii\data\ActiveDataProvider;

/**
 * Trait StorageItem
 * @package app\modules\storage\traits
 *
 * @property mixed $primaryKey
 */
trait StorageItem
{
    /**
     * @return ActiveDataProvider
     */
    public function getStorageDataProvider()
    {
        return new ActiveDataProvider([
            'query' => StorageHasItem::find()->andWhere(['item_id' => $this->primaryKey]),
        ]);
    }
}