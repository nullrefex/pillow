<?php

use rmrevin\yii\fontawesome\FA;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\storage\models\StorageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('storage', 'Storages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('storage', 'Create Storage'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {items} {update} {delete} {clear}',
                'buttons' => [
                    'items' => function ($key, $model) {
                        return Html::a(FA::i(FA::_LIST), ['items', 'id' => $model->id]);
                    },
                    'clear' => function ($key, $model) {
                        return Html::a(FA::i(FA::_BAN), ['clear', 'id' => $model->id], [
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to clear this item?'),
                            'data-method' => 'post',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
