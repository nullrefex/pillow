<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('product', 'Products');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'item.identifier',
            'item.name',
            'qty',
            /**
             * [
             * 'class' => 'yii\grid\ActionColumn',
             * 'template' => '{view} {update}',
             * ],
             **/
        ],
    ]); ?>

</div>

