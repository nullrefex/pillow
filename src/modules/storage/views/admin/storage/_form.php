<?php

use app\modules\eav\models\Attribute;
use app\modules\eav\widgets\Attributes;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Storage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="storage-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sort_order')->textInput() ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('storage', 'Create') : Yii::t('storage', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
