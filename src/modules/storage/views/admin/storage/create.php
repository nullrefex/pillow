<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Storage */

$this->title = Yii::t('storage', 'Create Storage');
$this->params['breadcrumbs'][] = ['label' => Yii::t('storage', 'Storages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('storage', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
