<?php

namespace app\modules\storage\migrations;

use app\modules\storage\models\Storage;
use yii\db\Migration;

class M170515213040Storage__add_storage extends Migration
{
    public function safeUp()
    {
        $list = [
            'Склад №1',
        ];
        foreach ($list as $index => $item) {
            $this->createStorage($item, $index + 1);
        }
    }

    public function createStorage($name, $sortOrder)
    {
        $model = new Storage();
        $model->name = $name;
        $model->sort_order = $sortOrder;
        $model->save();
    }

    public function safeDown()
    {
        $this->truncateTable('{{%storage_has_item}}');
        $this->truncateTable('{{%storage}}');
    }
}
