<?php

namespace app\modules\storage\migrations;

use yii\db\Migration;

class M170515213039Storage__add_storage_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%storage}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'options' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'sort_order' => $this->integer(),
        ]);

        $this->createTable('{{%storage_has_item}}', [
            'storage_id' => $this->integer(),
            'item_id' => $this->integer(),
            'qty' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('storage_has_item_pk', '{{%storage_has_item}}', [
            'storage_id',
            'item_id',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%storage_has_item}}');
        $this->dropTable('{{%storage}}');
    }
}
