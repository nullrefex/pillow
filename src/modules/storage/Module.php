<?php

namespace app\modules\storage;

use app\components\EntityManager;
use nullref\core\interfaces\IAdminModule;
use rmrevin\yii\fontawesome\FA;
use Yii;
use yii\base\Module as BaseModule;

/**
 * storage module definition class
 *
 * @property EntityManager $itemManager
 *
 */
class Module extends BaseModule implements IAdminModule
{
    const MODULE_ID = 'storage';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\storage\controllers';

    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('storage', 'Storage list'),
            'url' => ['/storage/admin/storage'],
            'icon' => FA::_ARCHIVE,
        ];
    }
}
