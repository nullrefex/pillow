<?php

namespace app\modules\storage\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Storage]].
 *
 * @see Storage
 */
class StorageQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Storage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Storage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
