<?php

namespace app\modules\storage\models;

use app\modules\eav\models\attribute\Option;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "storage_has_vendor".
 *
 * @property int $vendor_id
 * @property int $storage_id
 *
 * @property Option $vendor
 * @property Storage $storage
 */
class StorageHasVendor extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage_has_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'storage_id'], 'required'],
            [['vendor_id', 'storage_id'], 'integer'],
            [['vendor_id', 'storage_id'], 'unique', 'targetAttribute' => ['vendor_id', 'storage_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => Yii::t('storage', 'Vendor ID'),
            'storage_id' => Yii::t('storage', 'Storage ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::class, ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Option::class, ['id' => 'vendor_id']);
    }
}
