<?php

namespace app\modules\storage\models;

use nullref\useful\behaviors\JsonBehavior;
use nullref\useful\traits\Mappable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%storage}}".
 *
 * @property int $id
 * @property string $name
 * @property string $options
 * @property int $created_at
 * @property int $updated_at
 * @property int $sort_order
 *
 * @property StorageHasItem $items
 *
 */
class Storage extends ActiveRecord
{
    use Mappable;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage}}';
    }

    /**
     * @inheritdoc
     * @return StorageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return (new StorageQuery(get_called_class()))->orderBy(['sort_order' => SORT_ASC]);
    }

    /**
     * @param ActiveRecord $item
     * @return bool
     */
    public static function recalculateTotal(ActiveRecord $item)
    {
        $item->setAttributes(['qty' => static::getTotal($item)]);

        return $item->save(false, ['qty']);
    }

    /**
     * @param ActiveRecord $item
     * @return int
     */
    public static function getTotal(ActiveRecord $item)
    {
        return intval(StorageHasItem::find()->andWhere(['item_id' => $item->primaryKey])->sum('qty'));
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            'json' => [
                'class' => JsonBehavior::className(),
                'fields' => ['options'],
            ],
        ];
    }

    /**
     * @param ActiveRecord $item
     * @param int $qty
     * @param bool $recalculateTotal
     * @return bool
     */
    public function setQty(ActiveRecord $item, $qty, $recalculateTotal = true)
    {
        $storageHasItem = StorageHasItem::findOne(['storage_id' => $this->id, 'item_id' => $item->primaryKey]);
        if ($storageHasItem === null) {
            $storageHasItem = new StorageHasItem();
            $storageHasItem->setAttributes([
                'storage_id' => $this->id,
                'item_id' => $item->primaryKey,
            ]);
        }
        $storageHasItem->qty = intval($qty);

        $save = $storageHasItem->save();

        if ($recalculateTotal) {
            $save &= static::recalculateTotal($item);
        }
        return $save;
    }

    /**
     * @param ActiveRecord $item
     * @return int
     */
    public function getQty(ActiveRecord $item)
    {
        $storageHasItem = StorageHasItem::findOne(['storage_id' => $this->id, 'item_id' => $item->primaryKey]);
        if ($storageHasItem === null) {
            return 0;
        }

        return intval($storageHasItem->qty);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['options'], 'safe'],
            [['created_at', 'updated_at', 'sort_order'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('storage', 'ID'),
            'name' => Yii::t('storage', 'Name'),
            'options' => Yii::t('storage', 'Options'),
            'created_at' => Yii::t('storage', 'Created At'),
            'updated_at' => Yii::t('storage', 'Updated At'),
            'sort_order' => Yii::t('storage', 'Sort Order'),
            'telephone' => Yii::t('storage', 'Telephone'),
        ];
    }

    /**
     * Update all quantities from storage to 0
     */
    public function clear()
    {
        StorageHasItem::updateAll(['qty' => 0], ['storage_id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(StorageHasItem::class, ['storage_id' => 'id']);
    }

}
