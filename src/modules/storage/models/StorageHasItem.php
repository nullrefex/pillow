<?php

namespace app\modules\storage\models;

use app\modules\storage\helpers\Helper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%storage_has_item}}".
 *
 * @property int $storage_id
 * @property int $item_id
 * @property int $qty
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Storage $storage
 * @property ActiveRecord $item
 */
class StorageHasItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_has_item}}';
    }

    /**
     * @param mixed $condition
     * @return static
     */
    public static function findOne($condition)
    {
        if (is_string($condition)) {
            $ids = explode(':', $condition);
            if (count($ids) === 2) {
                return parent::findOne(['item_id' => $ids[0], 'storage_id' => $ids[1]]);
            }
        }
        return parent::findOne($condition);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_id', 'item_id', 'qty', 'created_at', 'updated_at'], 'integer'],
            [['storage_id', 'item_id', 'qty'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'storage_id' => Yii::t('storage', 'Storage ID'),
            'item_id' => Yii::t('storage', 'Item ID'),
            'qty' => Yii::t('storage', 'Qty'),
            'created_at' => Yii::t('storage', 'Created At'),
            'updated_at' => Yii::t('storage', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Helper::getItemManager()->getModel()::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id'])->orderBy('storage.sort_order');
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->item_id . ':' . $this->storage_id;
    }


}
