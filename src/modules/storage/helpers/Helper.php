<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\storage\helpers;


use app\components\EntityManager;
use app\modules\storage\Module;
use Yii;

class Helper
{
    /**
     * @return null|\yii\base\Module|\app\modules\storage\Module
     */
    public static function getModule()
    {
        return Yii::$app->getModule(Module::MODULE_ID);
    }

    /**
     * @return null|EntityManager
     */
    public static function getItemManager()
    {
        return self::getModule()->itemManager;
    }
}