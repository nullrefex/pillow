<?php

namespace app\modules\product\migrations;

use app\modules\eav\helpers\Migration as EavHelper;
use app\modules\eav\models\Attribute;
use yii\db\Migration;

class M170520175643Product__setup_eav extends Migration
{
    public function safeUp()
    {
        return EavHelper::createSet('product', 'Product', [
            [
                'attributes' => [
                    'name' => 'Виробник',
                    'code' => 'vendor',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 0,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Артикул',
                    'code' => 'sku',
                    'type' => Attribute::TYPE_STRING,
                    'sort_order' => 1,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Колір',
                    'code' => 'color',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 2,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Розмір',
                    'code' => 'size',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 3,
                ],
                'config' => [
                    'show_in_grid' => true,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Матеріал',
                    'code' => 'material',
                    'type' => Attribute::TYPE_TEXT,
                    'sort_order' => 4,
                ],
            ],
            [
                'attributes' => [
                    'name' => 'Країна виробника',
                    'code' => 'vendor_country',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 5,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Опис',
                    'code' => 'description',
                    'type' => Attribute::TYPE_TEXT,
                    'sort_order' => 6,
                ],
            ],
            [
                'attributes' => [
                    'name' => 'Сезон',
                    'code' => 'season',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 7,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Стиль',
                    'code' => 'style',
                    'type' => Attribute::TYPE_OPTION,
                    'sort_order' => 8,
                ],
                'options' => [],
            ],
            [
                'attributes' => [
                    'name' => 'Ціна закупки',
                    'code' => 'base_price',
                    'type' => Attribute::TYPE_DECIMAL,
                    'sort_order' => 9,
                ],
            ],
            [
                'attributes' => [
                    'name' => 'Ціна',
                    'code' => 'price',
                    'type' => Attribute::TYPE_DECIMAL,
                    'sort_order' => 10,
                ],
                'config' => [
                    'show_in_grid' => true,
                ],
            ],
            [
                'attributes' => [
                    'name' => 'Фото',
                    'code' => 'photo',
                    'type' => Attribute::TYPE_STRING,
                    'sort_order' => 11,
                ],
                'config' => [
                    'show_in_grid' => true,
                    'image' => true,
                ],
            ],
        ]);
    }

    /**
     *
     */
    public function safeDown()
    {
        EavHelper::deleteSet('product');
    }
}
