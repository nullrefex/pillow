<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

class M170520174350Product__create_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'identifier' => $this->string()->unique(),
            'name' => $this->string(),
            'qty' => $this->integer()->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
