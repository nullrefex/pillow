<?php

namespace app\modules\product\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * ProductSearch represents the model behind the search form about `app\modules\product\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['identifier', 'name', 'qty'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'class' => Sort::class,
                'attributes' => [
                    'qty' => [
                        'asc' => ['qty' => SORT_ASC],
                        'desc' => ['qty' => SORT_DESC],
                    ],
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        foreach ($this->eav->getAttributes() as $key => $value) {

            $valueModel = $this->eav->getAttributeModel($key)->createValue();

            $valueModel->setScenario('search');
            $valueModel->load(['value' => $value], '');
            if ($valueModel->validate(['value'])) {
                $valueModel->addJoin($query, Product::tableName());
                $valueModel->addWhere($query);
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if (isset($this->qty['from']) && $this->qty['from'] !== '') {
            $query->andFilterWhere(['>=', 'qty', intval($this->qty['from'])]);
        }
        if (isset($this->qty['to']) && $this->qty['to'] !== '') {
            $query->andFilterWhere(['<=', 'qty', intval($this->qty['to'])]);
        }

        $query->andFilterWhere(['like', 'identifier', $this->identifier])
            ->andFilterWhere(['like', 'name', $this->name]);

        $query->groupBy('id');

        return $dataProvider;
    }
}
