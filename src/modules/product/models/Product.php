<?php

namespace app\modules\product\models;

use app\modules\category\models\Category;
use app\modules\eav\behaviors\Entity;
use app\modules\eav\models\attribute\Set;
use app\modules\eav\models\Entity as EntityModel;
use app\modules\storage\traits\StorageItem;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property string $identifier
 * @property string $name
 * @property integer $qty
 *
 *
 * @property EntityModel $eav
 * @property Category[] $categories
 *
 */
class Product extends ActiveRecord
{
    const PHOTOS_DIR = '/photos/';
    use StorageItem;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'eav' => [
                'class' => Entity::class,
                'entity' => function () {
                    return new EntityModel([
                        'sets' => [
                            self::getDb()->cache(function () {
                                return Set::findOne(['code' => 'product']);
                            }, null),
                        ],
                    ]);
                },
            ],
            'manyToMany' => [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'categoriesList' => 'categories',
                ],
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('{{%product_has_category}}', ['product_id' => 'id'])->alias('categories');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identifier', 'name'], 'string', 'max' => 255],
            [['identifier'], 'unique'],
            [['qty'], 'integer'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->qty < 0) {
            $this->qty = 0;
        }
        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'identifier' => Yii::t('product', 'Identifier'),
            'name' => Yii::t('product', 'Name'),
            'qty' => Yii::t('product', 'Qty'),
        ];
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        if (!$this->eav->photo && !$this->isNewRecord) {
            return self::PHOTOS_DIR . trim($this->sku) . '.jpg';
        }
        return $this->eav->photo;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        $photo = $this->getPhoto();
        if (strpos($photo, 'http') === 0) {
            return $photo;
        }
        return Yii::$app->params['url'] . $photo;
    }

    /**
     * @return bool
     */
    public function hasPhoto()
    {
        $photo = $this->getPhoto();
        if (strpos($photo, 'http') === 0) {
            return true;
        }
        return file_exists(Yii::getAlias('@webroot' . $photo));
    }
}
