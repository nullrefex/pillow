<?php

namespace app\modules\product\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[ProductHasCategory]].
 *
 * @see ProductHasCategory
 */
class ProductHasCategoryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return ProductHasCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductHasCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
