<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2018 NRE
 */


namespace app\modules\product\models\service;


use app\modules\eav\models\Attribute;
use app\modules\eav\models\Value;
use app\modules\eav\models\ValueQuery;
use app\modules\product\models\Product;
use yii\base\Model;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;
use yii\db\QueryInterface;
use yii\helpers\ArrayHelper;

class BulkUpdate extends Model
{
    public $property;

    public $process;

    public $value;

    /**
     * @param Product $model
     * @return mixed
     */
    public static function getPropertiesMap($model)
    {
        return ArrayHelper::map($model->eav->getAttributesConfig(), 'code', 'name');
    }

    public function rules()
    {
        return [
            ['property', 'required'],
            ['value', 'required'],
            ['process', 'safe'],
        ];
    }

    /**
     * @param ActiveQuery|QueryInterface $query
     * @param Product $model
     *
     * @throws \yii\db\Exception
     * @return  boolean
     */
    public function execute($query, $model)
    {
        $ids = $query->select('product.id')->column();

        $attribute = $model->eav->getAttributeModel($this->property);

        $updater = $this->getUpdater($this->value, $attribute);

        $valueClass = $attribute->getValueClass();

        /** @var ValueQuery $valueQuery */
        $valueQuery = $valueClass::find();

        $valueQuery->andWhere(['entity_id' => $ids, 'attribute_id' => $attribute->id]);

        /** @var Value $valueModel */
        foreach ($valueQuery->each() as $valueModel) {
            $newValue = call_user_func_array($updater, [$valueModel->value]);
            $valueModel->value = $newValue;
            $valueModel->save(false, ['value']);
            TagDependency::invalidate(\Yii::$app->cache, $valueModel->getCacheKey());
        }
        return true;
    }

    /**
     * @param $newValue
     * @param Attribute $attribute
     * @return \Closure
     */
    protected function getUpdater($newValue, $attribute)
    {
        /**
         * Return new value by default
         */
        $func = $defaultFunc = function () use ($newValue) {
            return $newValue;
        };

        /**
         * Check if number type
         */
        if (in_array($attribute->type, [Attribute::TYPE_DECIMAL, Attribute::TYPE_INT])) {
            /**
             * Default set old value
             * @param $oldValue
             * @return mixed
             */
            $func = function ($oldValue) {
                return $oldValue;
            };

            /**
             * If new value start from '*' we need to multiple current value
             */
            if (strpos($newValue, '*') === 0) {
                $number = str_replace('*', '', $newValue);

                if (is_numeric($number)) {
                    $func = function ($oldValue) use ($number) {
                        return ceil($oldValue * $number);
                    };
                }
            }

            if (is_numeric($newValue)) {
                $func = $defaultFunc;
            }
        }

        return $func;
    }

}