<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\models\service;


use app\modules\product\models\Product;
use app\modules\storage\models\StorageHasItem;
use yii\data\ActiveDataProvider;

class QtyChecker extends Product
{
    public $s_qty;

    public function getWrongQtyDataProvider()
    {
        $subquery = self::find()
            ->select(['product.*', 'SUM(storage.qty) as s_qty'])
            ->innerJoin(['storage' => StorageHasItem::tableName()], 'product.id = storage.item_id')
            ->groupBy('product.id');

        $query = self::find()->from(['table' => $subquery])->andWhere('qty <> s_qty');

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}