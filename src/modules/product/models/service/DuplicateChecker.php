<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\models\service;


use app\modules\eav\models\Attribute;
use app\modules\eav\models\value\StringValue;
use app\modules\product\models\ProductSearch;
use Yii;
use yii\base\Model;

class DuplicateChecker extends Model
{
    /**
     * @return array
     */
    public function getDuplicates()
    {
        $sql = <<<SQL
SELECT *
FROM (
       SELECT
         product.id,
         product.identifier,
         Concat(trim(sku.value), '-', trim(size_option.value)) AS new_identifier,
         qty
       FROM product
         INNER JOIN eav_entity_value_string AS `sku` ON `sku`.entity_id = product.id AND sku.attribute_id = 1
         INNER JOIN eav_entity_value_int AS `size` ON `size`.entity_id = product.id AND size.attribute_id = 6
         INNER JOIN eav_attribute_option AS `size_option`
           ON `size_option`.id = size.value AND size_option.attribute_id = 6
     ) AS view

WHERE identifier <> new_identifier
SQL;

        $cmd = Yii::$app->db->createCommand($sql);

        return $cmd->queryAll();

    }


    /**
     * @return array
     */
    public function getUniqueSku(): array
    {
        return StringValue::find()
            ->select('value')
            ->where(['attribute_id' => Attribute::findOne(['code' => 'sku'])->id])
            ->groupBy('value')->column();
    }

    /**
     * @param $sku
     * @return \yii\data\ActiveDataProvider
     */
    public function getProductsBySku($sku)
    {
        $model = new ProductSearch();
        $dataProvider = $model->search(['ProductSearch' => ['sku' => $sku]]);

        return $dataProvider;
    }
}