<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\models\service;


use app\modules\category\models\Category;
use app\modules\product\models\Product;
use app\modules\product\models\ProductHasCategory;
use yii\base\Model;
use yii\helpers\Html;

class CategoryBinder extends Model
{
    public function rules()
    {
        return [
            ['edit', 'boolean'],
        ];
    }

    public $log = [];

    public $totalCount = 0;

    public $products = [];

    public $debug = false;

    public $edit = false;

    public function process()
    {
        if ($this->edit) {
            ProductHasCategory::deleteAll();
        }
        $ids = Product::find()->select('id')->asArray()->column();
        $this->products = array_combine($ids, array_fill(0, count($ids), 0));

        /** @var Category[] $categories */
        $categories = Category::find()->andWhere(['parent_id' => Category::ROOT_PARENT])->all();

        foreach ($categories as $category) {
            $this->processCategory($category);
        }

        $notFound = [];
        $duplicates = [];

        foreach ($this->products as $id => $qty) {
            if ($qty == 0) {
                $notFound[] = $id;
            } elseif ($qty > 1) {
                $duplicates[] = $id;
            }
        }
        $this->log('Дублікати(' . count($duplicates) . '):');
        foreach (Product::find()->andWhere(['id' => $duplicates])->asArray()->all() as $product) {
            if (isset($this->products[$product['id']])) {
                $this->products[$product['id']]++;
            }
            $this->log('     - ' . Html::a($product['identifier'] . ' -  ' . $product['name'], ['/product/admin/product/update', 'id' => $product['id']], ['target' => '_blank']));
        }
        $this->log('Не знайдено(' . count($notFound) . '):');
        foreach (Product::find()->andWhere(['id' => $notFound])->asArray()->all() as $product) {
            if (isset($this->products[$product['id']])) {
                $this->products[$product['id']]++;
            }
            $this->log('     - ' . Html::a($product['identifier'] . ' -  ' . $product['name'], ['/product/admin/product/update', 'id' => $product['id']], ['target' => '_blank']));
        }
    }

    /**
     * @param Category $category
     * @param array $conditions
     * @param string $offset
     */
    public function processCategory($category, $conditions = [], $offset = '')
    {
        $conditions = array_merge($conditions, $category->getBindConditions());
        if ($category->children) {
            $this->log($offset . Html::a($category->title, ['/category/admin/default/update', 'id' => $category->id], ['target' => '_blank']));
            /** @var Category $child */
            foreach ($category->children as $child) {
                $this->processCategory($child, $conditions, $offset . '     ');
            }
        } else {
            if (count($conditions)) {
                $products = Category::findProducts($conditions, $this);
                //$this->log(print_r($conditions,true));
                $count = count($products);
                $this->totalCount += $count;
                $this->log($offset . Html::a($category->title, ['/category/admin/default/update', 'id' => $category->id], ['target' => '_blank']) . '(' . $count . ')');

                foreach ($products as $product) {
                    if ($this->edit) {
                        $relation = new ProductHasCategory();
                        $relation->product_id = $product['id'];
                        $relation->category_id = $category->id;
                        $relation->save(false);
                    }
                    if (isset($this->products[$product['id']])) {
                        $this->products[$product['id']]++;
                    }
                    $this->log($offset . '     - ' . Html::a($product['identifier'] . ' -  ' . $product['name'], ['/product/admin/product/update', 'id' => $product['id']], ['target' => '_blank']));
                }
            }
        }
    }

    public function log($msg)
    {
        $this->log[] = $msg;
    }

    public function getLogs()
    {
        return implode(PHP_EOL, $this->log);
    }
}