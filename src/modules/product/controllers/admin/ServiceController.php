<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\product\controllers\admin;


use app\modules\eav\models\Attribute;
use app\modules\product\models\ProductSearch;
use app\modules\product\models\service\BulkUpdate;
use app\modules\product\models\service\CategoryBinder;
use app\modules\product\models\service\DuplicateChecker;
use app\modules\product\models\service\QtyChecker;
use app\modules\storage\models\StorageHasVendor;
use nullref\core\interfaces\IAdminController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class ServiceController extends Controller implements IAdminController
{
    public function actionCategoryBind()
    {
        $model = new CategoryBinder();

        $model->load(Yii::$app->request->post());
        $model->process();

        return $this->render('category-bind', [
            'model' => $model,
        ]);
    }
    /**
     * @return string
     */
    public function actionCheckDuplicates()
    {
        ini_set('memory_limit', -1);
        set_time_limit(-1);
        $model = new DuplicateChecker();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        }

        return $this->render('check-duplicates', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionQty()
    {
        $model = new QtyChecker();

        return $this->render('qty', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionVendors()
    {
        $subquery = Attribute::findOne(['code' => 'vendor'])->getOptions();

        $query = StorageHasVendor::find()
            ->from([StorageHasVendor::tableName() => 'storage_has_vendor'])
            ->select(['storage_has_vendor.*', 'vendor.id as vendor_id'])
            ->rightJoin(['vendor' => $subquery], 'vendor.id = storage_has_vendor.vendor_id')
            ->with(['vendor', 'storage']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('vendors', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionBulkUpdate()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new BulkUpdate();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->execute($dataProvider->query, $searchModel)) {
                Yii::$app->session->setFlash('success', Yii::t('product', 'Products were updated'));
            }
        }

        return $this->render('bulk-update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}