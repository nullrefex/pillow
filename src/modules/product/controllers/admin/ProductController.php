<?php

namespace app\modules\product\controllers\admin;

use app\modules\product\models\Product;
use app\modules\product\models\ProductSearch;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageHasItem;
use nullref\core\interfaces\IAdminController;
use nullref\useful\actions\MultipleDeleteAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionPhoto()
    {
        return $this->render('photo');
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionStorage($id)
    {
        /** @var Product $model */
        $model = $this->findModel($id);

        $dataProvider = $model->getStorageDataProvider();

        return $this->render('storage', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return array
     */
    public function actions()
    {
        return [
            'delete-multiple' => [
                'class' => MultipleDeleteAction::class,
            ],
            'storage-qty' => [
                'class' => 'mcms\xeditable\XEditableAction',
                'modelclass' => StorageHasItem::class,
            ],
        ];
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCreateStorageItem($id)
    {
        $product = $this->findModel($id);

        $model = new StorageHasItem();

        $model->item_id = $product->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Storage::recalculateTotal($product);
            return $this->redirect(['storage', 'id' => $id]);
        }
        return $this->render('create-storage-item', [
            'model' => $model,
        ]);
    }


    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
