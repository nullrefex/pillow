<?php

namespace app\modules\product;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * product module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    const MODULE_ID = 'order';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\product\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('product', 'Catalog'),
            'icon' => 'archive',
            'order' => 0,
            'items' => [
                [
                    'label' => Yii::t('product', 'Products'),
                    'icon' => 'archive',
                    'url' => '/product/admin/product',
                ],
                [
                    'label' => Yii::t('product', 'Photos'),
                    'icon' => 'photo',
                    'url' => '/product/admin/product/photo',
                ],
                [
                    'label' => Yii::t('product', 'Service'),
                    'icon' => 'cogs',
                    'items' => [
                        [
                            'label' => Yii::t('product', 'Category bind'),
                            'icon' => 'archive',
                            'url' => '/product/admin/service/category-bind',
                        ],
                        [
                            'label' => Yii::t('product', 'Bulk update'),
                            'icon' => 'archive',
                            'url' => '/product/admin/service/bulk-update',
                        ],/*
                        [
                            'label' => Yii::t('product', 'Qty check'),
                            'icon' => 'archive',
                            'url' => '/product/admin/service/qty',
                        ],
                        [
                            'label' => Yii::t('product', 'Vendors to storage'),
                            'icon' => 'archive',
                            'url' => '/product/admin/service/vendors',
                        ],*/
                    ],
                ]
            ]
        ];
    }
}
