<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model \app\modules\product\models\service\CategoryBinder */
?>

<div>
    <div class="row">
        <div class="col-lg-12">
            <h1>Групування товарів (<?= $model->totalCount ?>)</h1>
        </div>
    </div>
    <pre><?= $model->getLogs(); ?></pre>

    <div class="well">
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($model, 'edit')->hiddenInput(['value' => true])->label(false) ?>
        <?= Html::submitButton('Обновити', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end() ?>
    </div>
    <br>
</div>
