<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var \app\modules\product\models\service\DuplicateChecker $model
 */

use app\modules\eav\helpers\Grid as EavGrid;
use app\modules\product\models\Product;
use yii\data\ArrayDataProvider;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$tmpProduct = new Product();
$duplicates = $model->getDuplicates();

$removeUrlTemplate = yii\helpers\Url::to([
    '/product/admin/product/delete-multiple',
    'ids' => 'ITEM_IDS',
]);

$this->registerJs(<<<JS
var removeUrl = "$removeUrlTemplate";

var documentItemCheckboxes = jQuery('.itemCheckbox');
var removeBtn = jQuery('.remove-btn');

documentItemCheckboxes.on('change', function (e) {
    var activeItems = documentItemCheckboxes.filter(':checked');
    if (activeItems.length > 0) {
        removeBtn.show();
        var selectedIds = [];
        activeItems.each(function (i, el) {
            selectedIds.push(jQuery(el).val());
        });
        removeBtn.attr('href', removeUrl.replace('ITEM_IDS', selectedIds.join(',')));
    } else {
        removeBtn.attr('href', '#');
        removeBtn.hide();
    }
});


JS
)

?>

<div class="service-check-duplicates">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Yii::t('app', 'Duplicates') . ' (' . count($duplicates) . ')' ?>
            </h1>
        </div>
    </div>


    <p>
        <?= Html::a(Yii::t('product', 'Delete selected'), [''], [
            'class' => 'btn btn-danger remove-btn',
            'style' => 'display:none',
            'data-method' => 'post',
        ]) ?>
    </p>
    <?php foreach ($duplicates as $item): ?>
        <?php
        $badProduct = Product::findOne(['identifier' => $item['identifier']]);
        $goodProduct = Product::findOne(['identifier' => $item['new_identifier']]);
        $items = [
            $badProduct->id => $badProduct,
        ];

        if ($goodProduct) {
            $items[$goodProduct->id] = $goodProduct;
        }

        $dataProvider = new ArrayDataProvider([
            'modelClass' => Product::class,
            'allModels' => $items]);
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'columns' => array_merge([
                [
                    'class' => CheckboxColumn::class,
                    'checkboxOptions' => [
                        'class' => 'itemCheckbox'
                    ],
                ],
                'identifier',
                'name',
            ], EavGrid::getGridColumnsInternal($tmpProduct->eav), [
                'qty',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => '/product/admin/product',
                    'template' => '{view} {update} {storage} {delete}',
                    'buttons' => [
                        'storage' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('eav', 'Storage'),
                                'aria-label' => Yii::t('eav', 'Storage'),
                                'data-pjax' => '0',
                            ];
                            $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-list"]);
                            return Html::a($icon, $url, $options);
                        },
                    ],
                ],
            ]),
        ]); ?>
    <?php endforeach ?>

    <p>
        <?= Html::a(Yii::t('product', 'Delete selected'), [''], [
            'class' => 'btn btn-danger remove-btn',
            'style' => 'display:none',
            'data-method' => 'post',
        ]) ?>
    </p>

</div>