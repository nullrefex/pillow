<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\grid\GridView;

?>

<div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Yii::t('app', 'Vendors to storage') ?>
            </h1>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'vendor.value',
            'storage.name',
        ]
    ]) ?>
</div>
