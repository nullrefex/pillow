<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2018 NRE
 *
 * @var $model BulkUpdate
 */

use app\modules\eav\helpers\Grid as EavGrid;
use app\modules\eav\widgets\ActiveRangeInputGroup;
use app\modules\product\models\service\BulkUpdate;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Bulk update');

?>
<div class="service-bulk-update">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= $this->title ?>
            </h1>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            [
                'class' => CheckboxColumn::class,
                'checkboxOptions' => [
                    'class' => 'itemCheckbox'
                ],
            ],
            'identifier',
            'name',
        ], EavGrid::getGridColumns($searchModel), [
            [
                'attribute' => 'qty',
                'contentOptions' => ['class' => 'storage-column'],
                'filter' => ActiveRangeInputGroup::widget([
                    'attributeFrom' => 'qty[from]',
                    'attributeTo' => 'qty[to]',
                    'model' => $searchModel,
                    'options' => [
                        'style' => 'min-width: 100px',
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {storage} {delete}',
                'controller' => 'admin/product',
                'buttons' => [
                    'storage' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('eav', 'Storage'),
                            'aria-label' => Yii::t('eav', 'Storage'),
                            'data-pjax' => '0',
                        ];
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-list"]);
                        return Html::a($icon, $url, $options);
                    },
                ],
            ],
        ]),
    ]); ?>


    <div class="well">
        <?php $form = ActiveForm::begin() ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'property')->dropDownList(BulkUpdate::getPropertiesMap($searchModel)) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'value')->textarea() ?>
            </div>
        </div>

        <p>
            <?= Html::submitInput(Yii::t('product', 'Run'), ['class' => 'btn btn-primary']) ?>
        </p>

        <?php ActiveForm::end() ?>
    </div>
    <br>

</div>
