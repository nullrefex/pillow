<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\web\View $this
 * @var \app\modules\product\models\service\QtyChecker $model
 */

use app\modules\product\models\Product;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$dataProvider = $model->getWrongQtyDataProvider();

$tmpProduct = new Product();
?>

<div class="service-check-duplicates">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Yii::t('app', 'Wrong qty') ?>
            </h1>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => CheckboxColumn::class,
                'checkboxOptions' => [
                    'class' => 'itemCheckbox'
                ],
            ],
            'identifier',
            'name',
            [
                'label' => Yii::t('product', 'Storage qty'),
                'attribute' => 's_qty',
            ],
            'qty',
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => '/product/admin/product',
                'template' => '{view} {update} {storage} {delete}',
                'buttons' => [
                    'storage' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('eav', 'Storage'),
                            'aria-label' => Yii::t('eav', 'Storage'),
                            'data-pjax' => '0',
                        ];
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-list"]);
                        return Html::a($icon, $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>
</div>