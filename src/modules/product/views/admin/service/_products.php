<?php

use app\modules\eav\helpers\Grid as EavGrid;
use app\modules\product\models\Product;
use yii\grid\GridView;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\eav\models\Entity $entity
 */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => array_merge([

        'identifier',
        'name',
    ], EavGrid::getGridColumnsInternal($entity), [
        'qty',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ]),
]); ?>

