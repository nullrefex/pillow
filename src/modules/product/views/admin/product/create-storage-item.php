<?php

use app\modules\storage\models\Storage;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yiiweb\View */
/* @var $model \app\modules\storage\models\StorageHasItem */

$this->title = Yii::t('product', 'Add storage item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('product', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('product', 'Products'), 'url' => ['storage', 'id' => $model->item_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('product', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>


    <div>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, 'storage_id')->dropDownList(Storage::getMap()) ?>

        <?= $form->field($model, 'qty')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('product', 'Create') : Yii::t('product', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
