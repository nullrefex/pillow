<?php

use app\modules\eav\helpers\Grid as EavGrid;
use app\modules\eav\widgets\ActiveRangeInputGroup;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('product', 'Products');
$this->params['breadcrumbs'][] = $this->title;


$removeUrlTemplate = yii\helpers\Url::to([
    '/product/admin/product/delete-multiple',
    'ids' => 'ITEM_IDS',
]);

$this->registerJs(<<<JS
var removeUrl = "$removeUrlTemplate";

var documentItemCheckboxes = jQuery('.itemCheckbox');
var removeBtn = jQuery('.remove-btn');

documentItemCheckboxes.on('change', function (e) {
    var activeItems = documentItemCheckboxes.filter(':checked');
    if (activeItems.length > 0) {
        removeBtn.show();
        var selectedIds = [];
        activeItems.each(function (i, el) {
            selectedIds.push(jQuery(el).val());
        });
        removeBtn.attr('href', removeUrl.replace('ITEM_IDS', selectedIds.join(',')));
    } else {
        removeBtn.attr('href', '#');
        removeBtn.hide();
    }
});


JS
)

?>
<div class="product-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('product', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a(Yii::t('product', 'Delete selected'), [''], [
            'class' => 'btn btn-danger remove-btn',
            'style' => 'display:none',
            'data-method' => 'post',
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            [
                'class' => CheckboxColumn::class,
                'checkboxOptions' => [
                    'class' => 'itemCheckbox'
                ],
            ],
            'identifier',
            'name',
        ], EavGrid::getGridColumns($searchModel), [
            [
                'attribute' => 'qty',
                'contentOptions' => ['class' => 'storage-column'],
                'filter' => ActiveRangeInputGroup::widget([
                    'attributeFrom' => 'qty[from]',
                    'attributeTo' => 'qty[to]',
                    'model' => $searchModel,
                    'options' => [
                        'style' => 'min-width: 100px',
                    ],
                ]),
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {storage} {delete}',
                'buttons' => [
                    'storage' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('eav', 'Storage'),
                            'aria-label' => Yii::t('eav', 'Storage'),
                            'data-pjax' => '0',
                        ];
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-list"]);
                        return Html::a($icon, $url, $options);
                    },
                ],
            ],
        ]),
    ]); ?>

    <p>
        <?= Html::a(Yii::t('product', 'Delete selected'), [''], [
            'class' => 'btn btn-danger remove-btn',
            'style' => 'display:none',
            'data-method' => 'post',
        ]) ?>
    </p>

</div>
