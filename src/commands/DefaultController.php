<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2018 NRE
 */


namespace app\commands;


use app\modules\documents\models\config\Import as ImportConfig;
use app\modules\documents\models\Document;
use app\modules\documents\models\document\Import;
use yii\console\Controller;
use yii\helpers\Console;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $document = new Import();
        $document->config_id = 5;
        $document->options = [
            'file_path' => 'https://www.stimma.com.ua/wp-content/plugins/saphali-export-yml/export.yml',
        ];

        $config = new ImportConfig();
        $config->type = Document::TYPE_IMPORT;
        $config->options = [
            'importer' => 'stimma_yml',
        ];

        $importer = $config->getImporter();
        Console::output('Create importer: ' . get_class($importer));
        Console::output('Import started');
        $rerun = false;
        $document->options = array_merge($document->options, ['rerun' => $rerun]);
        $importer->import($document);

    }
}