<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components\exporters;


use app\modules\documents\components\BaseExporter;
use app\modules\documents\models\Document;
use app\modules\eav\models\Attribute;
use app\modules\product\models\Product;
use Yii;
use yii\helpers\Console;

class TotalExporter extends BaseExporter
{
    public function getDocumentOptions()
    {
        return [
            'checkImage' => [
                'type' => self::OPTION_TYPE_DROPDOWN,
                'label' => 'Фільтр по фото',
                'items' => [
                    'withPhoto' => 'З фото',
                    'withoutPhoto' => 'Без фото',
                    'all' => 'Всі',
                ],
            ],
        ];
    }

    public function exportInternal(Document $document)
    {
        $name = time() . '_' . $this->getName();
        $dir = Yii::getAlias('@webroot/uploads');

        $dist = $dir . DIRECTORY_SEPARATOR . $name . '.xlsx';
        $src = Yii::getAlias('@webroot/export.xlsx');

        copy($src, $dist);

        $excel = \PHPExcel_IOFactory::createReader('Excel2007');

        /** @var \PHPExcel $objPHPExcel */
        $objPHPExcel = $excel->load($dist);
        $objPHPExcel->setActiveSheetIndex(0);

        $worksheet = $objPHPExcel->getActiveSheet();
        $offset = 3;


        $length = Product::find()->count();
        Console::output('Find ' . $length . ' products');

        $worksheet->insertNewRowBefore($offset, $length);


        $products = Product::find()->each(100);

        $index = 0;

        $vendor = Attribute::findOne(['code' => 'vendor']);
        $vendors = $vendor->getOptions()->indexBy('id')->select('value')->column();

        $style = Attribute::findOne(['code' => 'style']);
        $styles = $style->getOptions()->indexBy('id')->select('value')->column();


        $size = Attribute::findOne(['code' => 'size']);
        $sizes = $size->getOptions()->indexBy('id')->select('value')->column();

        $color = Attribute::findOne(['code' => 'color']);
        $colors = $color->getOptions()->indexBy('id')->select('value')->column();

        $season = Attribute::findOne(['code' => 'season']);
        $seasons = $season->getOptions()->indexBy('id')->select('value')->column();

        $vendor_country = Attribute::findOne(['code' => 'vendor_country']);
        $vendor_countries = $vendor_country->getOptions()->indexBy('id')->select('value')->column();

        $objPHPExcel->getActiveSheet()->getStyle('A2:O' . ($length + 1))->getFill()->applyFromArray([
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => [
                'rgb' => 'FFFFFF',
            ]
        ]);

        $checkImage = $this->getOptionValue($document, 'checkImage');

        foreach ($products as $item) {
            /** @var $item Product */
            if ($checkImage !== 'all') {
                if ($checkImage === 'withoutPhoto' && $item->hasPhoto()) {
                    continue;
                }
                if ($checkImage === 'withPhoto' && !$item->hasPhoto()) {
                    continue;
                }
            }
            Console::output('SKU: ' . $item->sku);
            $worksheet->setCellValue('B' . ($offset + $index), ($index + 1));
            $worksheet->setCellValue('B' . ($offset + $index), isset($vendors[$item->vendor]) ? $vendors[$item->vendor] : '');
            $worksheet->setCellValue('C' . ($offset + $index), $item->sku);
            $worksheet->setCellValue('D' . ($offset + $index), 'None');
            $worksheet->setCellValue('E' . ($offset + $index), isset($colors[$item->color]) ? $colors[$item->color] : '');
            $worksheet->setCellValue('F' . ($offset + $index), isset($sizes[$item->size]) ? $sizes[$item->size] : '');
            $worksheet->setCellValue('G' . ($offset + $index), $item->qty);
            $worksheet->setCellValue('H' . ($offset + $index), $item->name);
            $worksheet->setCellValue('I' . ($offset + $index), $item->material);
            $worksheet->setCellValue('J' . ($offset + $index), isset($vendor_countries[$item->vendor_country]) ? $vendor_countries[$item->vendor_country] : '');
            $worksheet->setCellValue('K' . ($offset + $index), $item->description);
            $worksheet->setCellValue('L' . ($offset + $index), isset($seasons[$item->season]) ? $seasons[$item->season] : '');
            $worksheet->setCellValue('M' . ($offset + $index), isset($styles[$item->style]) ? $styles[$item->style] : '');
            $worksheet->setCellValue('N' . ($offset + $index), $item->base_price, true)->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $worksheet->setCellValue('O' . ($offset + $index), $item->price_wholesale, true)->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $worksheet->setCellValue('P' . ($offset + $index), $item->price_retail, true)->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
            /** @var \PHPExcel_Cell_Hyperlink $hyperlink */
            $hyperlink = $worksheet->setCellValue('Q' . ($offset + $index), $item->getPhotoUrl(), true)->getHyperlink('P' . ($offset + $index));
            $hyperlink->setUrl($item->getPhotoUrl());
            $worksheet->setCellValue('R' . ($offset + $index), $item->keywords);
            $index++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($dist);

        $document->file_path = $dist;
        $document->save(false, ['file_path']);
    }

    public function getName()
    {
        return Yii::t('documents', 'Total exporter');
    }
}