<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\components\yml;


use app\modules\category\models\Category;
use app\modules\eav\models\Attribute;
use app\modules\product\models\Product;
use app\modules\product\models\ProductHasCategory;
use Yii;
use yii\base\Component;

class YmlExporter extends Component
{
    public $priceCoef = 1;
    public $saveToFile = true;

    public $options = [];

    /**
     * @param $ids
     */
    public function export($ids = false)
    {
        $ids = ($ids === false) ? [] : explode(',', $ids);

        $doc = new YmlDocument('pillow', '', '');

        $doc->currency('UAH', 1);

        $categories = Category::find()->asArray()->all();
        foreach ($categories as $row) {
            if (ProductHasCategory::find()->andWhere(['category_id' => $row['id']])->exists()
                || Category::find()->andWhere(['parent_id' => $row['id']])->exists()) {
                $doc->category($row['id'], $row['title'], $row['prom_id'], isset($row['parent_id']) && ($row['parent_id'] > 0) ? $row['parent_id'] : false);
            }
        }

        unset($categories);

        $query = Product::find();

        if ($ids) {
            $query->andWhere(['product.id' => $ids]);
        }

        foreach ($query->each() as $row) {
            $this->addItem($doc, $row);

        }
        if ($this->saveToFile) {
            $this->saveFile($doc);
        } else {
            header('Content-Type: text/xml');
            header('Content-Disposition: attachment; filename="feed.xml"');
            echo $doc->saveXML();
        }
    }

    public function init()
    {
        $this->initOptions();
        parent::init();
    }

    /**
     *
     */
    public function initOptions()
    {
        foreach ([
                     'vendor' => 'Производитель',
                     'style' => 'Стиль',
                     'color' => 'Цвет',
                     'size' => 'Размер',
                     'season' => 'Сезон',
                     'vendor_country' => 'Страна производитель',
                 ] as $key => $label) {

            $attribute = Attribute::findOne(['code' => $key]);
            $items = $attribute->getOptions()->indexBy('id')->select('value')->column();

            $this->options[$key] = [
                'label' => $label,
                'items' => $items,
            ];
        }

    }

    protected function getPrice($price)
    {
        return $price * $this->priceCoef;
    }

    protected function filterDescription($product_description)
    {
        return str_replace(
            [';asymp;', '&asymp;'],
            [';', ''],
            $product_description);
    }

    /**
     * @param YmlDocument $xml
     */
    protected function saveFile($xml)
    {
        $file = Yii::getAlias('@webroot/files/xml/feed.xml');

        $xml->save($file);

        Yii::$app->response->redirect('/files/xml/feed.xml');

    }

    /**
     * @param YmlDocument $doc
     * @param $row
     */
    private function addItem(YmlDocument $doc, Product $row)
    {
        $name = $row['name'] . ' ' . $row['sku'];

        if (isset($this->options['size']['items'][$row['size']])) {
            $size = $this->options['size']['items'][$row['size']];
            if (strpos($name, $size) === false) {
                $name = $row['name'] . ' ' . $size . ' ' . $row['sku'];
            }
        }

        $offer = $doc->product($row['price_retail'], 'UAH', $name);

        $prices = $doc->createElement('prices');
        $price = $doc->createElement('price');
        $value = $doc->createElement('value', $row['price_wholesale']);
        $quantity = $doc->createElement('quantity', ($row['qty'] > 0) ? $row['qty'] : 3);

        $price->appendChild($value);
        $price->appendChild($quantity);
        $prices->appendChild($price);
        $offer->appendChild($prices);

        foreach ($row->categories as $item) {
            $offer->add('categoryId', $item->id);
        }

        $offer->id($row['id']);

        if (($row['qty'] > 0)) {
            $offer->available(true);
        } else {
            $offer->available(false);
        }

        $offer->add('description', $this->filterDescription($row['description'] . PHP_EOL . $row['material']));

        if (isset($this->options['vendor']['items'][$row['vendor']])) {
            $offer->add('vendor', $this->options['vendor']['items'][$row['vendor']]);
        }
        $offer->add('vendorCode', $row['sku']);
        $offer->add('pickup', 'true');
        $offer->add('delivery', 'true');
        $offer->add('keywords', $row['keywords']);

        $offer->picture($row->getPhotoUrl());

        foreach ($this->options as $key => $option) {
            if (isset($option['items'][$row[$key]])) {
                $value = $option['items'][$row[$key]];
                $label = $option['label'];
                $offer->param($label, $value);
            }
        }
    }
}