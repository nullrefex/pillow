<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components\importers;


use app\modules\category\models\Category;
use app\modules\documents\components\BaseImporter;
use app\modules\documents\models\Document;
use app\modules\eav\models\Attribute;
use app\modules\product\models\Product;

class PromYmlImporter extends BaseImporter
{
    public $categories = [];

    public $missingVendorCode = 0;
    public $notFound = 0;
    public $total = 0;

    public function import(Document $document)
    {
        $doc = simplexml_load_file($document->file_path);

        $this->parseCategories($doc);

        $this->parseProducts($doc);

        print_r(PHP_EOL);
        print_r('Total: ' . $this->total . PHP_EOL);
        print_r('Missing vendor code: ' . $this->missingVendorCode . PHP_EOL);
        print_r('Not found: ' . $this->notFound . PHP_EOL);
    }

    /**
     * @param \SimpleXMLElement $doc
     */
    public function parseCategories(\SimpleXMLElement $doc)
    {
        $categories = $doc->xpath('//category');

        foreach ($categories as $childNode) {
            /** @var $childNode \SimpleXMLElement */
            $promId = (string)$childNode['id'];
            $promParentId = (string)$childNode['parentId'];
            $title = trim((string)$childNode);

            $record = [
                'data' => [
                    'promId' => $promId,
                    'promParentId' => $promParentId,
                    'title' => $title,
                ],
            ];
            $this->categories[$promId] = $record;

        }

        foreach ($this->categories as $id => $category) {
            $this->getCategoryModel($id);
        }
    }

    /**
     * @param $id
     * @return Category|null
     */
    public function getCategoryModel($id)
    {
        if (isset($this->categories[$id])) {
            if (isset($this->categories[$id]['model'])) {
                return $this->categories[$id]['model'];
            } else {
                $model = Category::findOne(['prom_id' => $id]);
                if ($model) {
                    $this->categories[$id]['model'] = $model;
                } else {
                    $this->categories[$id]['model'] = $this->createCategoryModel($this->categories[$id]['data']);
                }
                return $this->categories[$id]['model'];
            }
        }
        return null;
    }

    /**
     * @param $data
     * @return Category
     */
    public function createCategoryModel($data)
    {
        $model = new Category();
        $model->title = $data['title'];
        $model->prom_id = $data['promId'];
        if ($data['promParentId']) {
            $parentModel = $this->getCategoryModel($data['promParentId']);
            if ($parentModel) {
                $model->parent_id = $parentModel->id;
            }
        }
        $model->save();
        $this->categories[$data['promId']]['model'] = $model;
        return $model;
    }

    public function parseProducts(\SimpleXMLElement $doc)
    {
        $offers = $doc->xpath('//offer');
        foreach ($offers as $offer) {
            $children = (array)$offer->children();
            $this->total++;
            //print_r($offer->children());
            if (isset($children['vendorCode'])) {

                $vendorCode = (string)$children['vendorCode'];
                $product = $this->findProductByVendorCode($vendorCode);
                if (!$product) {
                    $this->notFound++;
                    print_r('*');
                } else {
                    print_r('.');
                }
            } else {
                print_r('*');
                $this->missingVendorCode++;
            }
            //$categoryId = (string)$children['categoryId'];
            //print_r($categoryId . PHP_EOL . $this->categories[$categoryId]['model']->title . PHP_EOL);
            //print_r($sku . PHP_EOL);
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Prom yml import';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'extensions' => [
                'type' => self::OPTION_TYPE_TEXT,
            ],
        ];
    }

    public function findProductByVendorCode($vendorCode)
    {
        $value = Attribute::findOne(['code' => 'sku'])->createValue();
        $value->value = $vendorCode;
        $query = Product::find();
        $value->addFilter($query, Product::tableName());

        return $query->one();
    }


}