<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\components\importers;

use app\modules\documents\components\ExcelImporter;
use app\modules\documents\models\Document;
use app\modules\documents\models\DocumentItem;
use app\modules\product\models\Product;
use app\modules\storage\models\Storage;
use app\traits\Importer;
use Yii;

class CatalogImporter extends ExcelImporter
{
    use Importer;

    public $optionFields = ['vendor', 'color', 'size', 'vendor_country', 'gender', 'season', 'style'];
    public $valueFields = ['sku', 'qty', 'material', 'description', 'keywords', 'base_price', 'price_wholesale', 'price_retail'];
    protected $storage = [];

    /**
     * @param Document $document
     */
    public function beforeImport(Document $document)
    {
        parent::beforeImport($document);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('documents', 'Catalog importer');
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), [
            'create_if_not_exist' => [
                'type' => self::OPTION_TYPE_CHECKBOX,
                'label' => Yii::t('documents', 'Create if not exist'),
            ],
            'extensions' => [
                'type' => self::OPTION_TYPE_TEXT,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return [
            'vendor' => Yii::t('documents', 'Vendor'),
            'sku' => Yii::t('documents', 'Sku'),
            //
            'color' => Yii::t('documents', 'Color'),
            'size' => Yii::t('documents', 'Size'),
            'qty' => Yii::t('documents', 'Qty'),
            'name' => Yii::t('documents', 'Name'),
            'material' => Yii::t('documents', 'Material'),
            'vendor_country' => Yii::t('documents', 'Vendor country'),
            'description' => Yii::t('documents', 'Description'),
            'season' => Yii::t('documents', 'Season'),
            'style' => Yii::t('documents', 'Style'),
            'base_price' => Yii::t('documents', 'Base Price'),
            'price_wholesale' => Yii::t('documents', 'Wholesale Price'),
            'price_retail' => Yii::t('documents', 'Retail Price'),
            'keywords' => Yii::t('documents', 'Keywords'),
            //
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->storage = Storage::find()->all();
    }

    /**
     * @param Document $document
     * @return bool
     * @throws \Exception
     */
    protected function load(Document $document)
    {
        if ($document->getDocumentItems()->count()) {
            DocumentItem::deleteAll(['document_id' => $document->id]);
        }
        return parent::load($document);
    }

    /**
     * @param $record
     * @param $index
     * @param Document $document
     */
    protected function modifyItem($record, $index, Document $document)
    {
        $data = $record->data;

        if (isset($data['sku']) && isset($data['size'])) {
            $sku = $data['sku'];
            $size = $data['size'];
            if ($sku && $size) {
                $identifier = $this->buildIdentifier($data);
                $data['identifier'] = $identifier;
                try {
                    /** @var Product $product */
                    $product = Product::findOne(['identifier' => $identifier]);
                    if ($product) {
                        $product = $this->updateProduct($product, $data);
                    } else {
                        $product = $this->createProduct($data);
                    }
                    if (!$product->save()) {
                        $record->error = implode(array_filter($product->getFirstErrors()), '; ');
                        $record->status = DocumentItem::STATUS_ERROR;
                        $record->save(false, ['status', 'error']);
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * @param Product $product
     * @param $data
     * @return Product
     */
    protected function updateProduct(Product $product, $data)
    {
        echo 'Update product ID:' . $product->id . ' Sku:' . $data['sku'] . PHP_EOL;

        foreach ($this->valueFields as $item) {
            if (isset($data[$item]) && $data[$item]) {
                $value = $data[$item];
                if (in_array($item, ['price', 'price_wholesale', 'price_retail'])) {
                    $value = trim($data[$item], 't');
                }
                $product->$item = $value;
            }
        }

        foreach ($this->optionFields as $item) {
            if (isset($data[$item]) && $data[$item]) {
                $value = $this->filterValue($data[$item]);
                $valueModel = $this->getAttributeOption($item, $value);
                $product->$item = $valueModel->id;
            }
        }
        return $product;
    }

    /**
     * @param $value
     * @return string
     */
    protected function filterValue($value)
    {
        return trim($value);
    }


    /**
     * @param $data
     * @return Product
     */
    protected function createProduct($data)
    {
        $product = new Product();
        echo 'Create product Sku:' . $data['sku'] . PHP_EOL;

        $product->identifier = $data['identifier'];
        if (isset($data['name'])) {
            $product->name = $data['name'];
        }

        foreach ($this->valueFields as $item) {
            if (isset($data[$item]) && $data[$item]) {
                $value = $data[$item];
                if (in_array($item, ['price', 'base_price'])) {
                    $value = trim($data[$item], 't');
                }
                $product->$item = $value;
            }
        }

        foreach ($this->optionFields as $item) {
            if (isset($data[$item]) && $data[$item]) {
                $value = $this->filterValue($data[$item]);
                $valueModel = $this->getAttributeOption($item, $value);
                $product->$item = $valueModel->id;
            }
        }

        return $product;
    }
}