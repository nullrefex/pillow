<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2018 NRE
 */


namespace app\components\importers;


use app\modules\category\models\Category;
use app\modules\documents\components\YmlImporter;
use app\modules\documents\models\Document;
use app\modules\eav\models\Attribute;
use app\modules\product\models\Product;
use app\modules\product\models\ProductHasCategory;
use app\traits\Importer;
use yii\helpers\Console;

class StimmaYmlImporter extends YmlImporter
{
    use Importer;

    public $categories = [];
    public $missingVendorCode = 0;
    public $notFound = 0;
    public $total = 0;

    public function getName()
    {
        return 'www.stimma.com.ua import';
    }

    public function import(Document $document)
    {
        $doc = $this->loadYmlFile($document);

        $this->parseCategories($doc);

        $this->parseProducts($doc);

        print_r(PHP_EOL);
        print_r('Total: ' . $this->total . PHP_EOL);
        print_r('Missing vendor code: ' . $this->missingVendorCode . PHP_EOL);
        print_r('Not found: ' . $this->notFound . PHP_EOL);
    }

    /**
     * @param \SimpleXMLElement $doc
     */
    public function parseCategories(\SimpleXMLElement $doc)
    {
        $categories = $doc->xpath('//category');

        foreach ($categories as $childNode) {
            /** @var $childNode \SimpleXMLElement */
            $promId = (string)$childNode['id'];
            $promParentId = (string)$childNode['parentId'];
            $title = trim((string)$childNode);

            $record = [
                'data' => [
                    'promId' => $promId,
                    'promParentId' => $promParentId,
                    'title' => $title,
                ],
            ];
            $this->categories[$promId] = $record;

        }

        foreach ($this->categories as $id => $category) {
            $this->getCategoryModel($id);
        }
    }

    /**
     * @param $id
     * @return Category|null
     */
    public function getCategoryModel($id)
    {
        if (isset($this->categories[$id])) {
            if (isset($this->categories[$id]['model'])) {
                return $this->categories[$id]['model'];
            } else {
                $model = Category::findOne(['prom_id' => $id]);
                if ($model) {
                    $this->categories[$id]['model'] = $model;
                } else {
                    $this->categories[$id]['model'] = $this->createCategoryModel($this->categories[$id]['data']);
                }
                return $this->categories[$id]['model'];
            }
        }
        return null;
    }

    /**
     * @param $data
     * @return Category
     */
    public function createCategoryModel($data)
    {
        $model = new Category();
        $model->title = $data['title'];
        $model->prom_id = $data['promId'];
        if ($data['promParentId']) {
            $parentModel = $this->getCategoryModel($data['promParentId']);
            if ($parentModel) {
                $model->parent_id = $parentModel->id;
            }
        }
        $model->save();
        $this->categories[$data['promId']]['model'] = $model;
        return $model;
    }

    public function parseProducts(\SimpleXMLElement $doc)
    {
        $offers = $doc->xpath('//offer');
        foreach ($offers as $offer) {
            $children = (array)$offer->children();

            foreach ((array)$offer->xpath('param') as $item) {
                $itemData = (array)$item;
                $children[$itemData['@attributes']['type']] = $itemData[0];
            }
            foreach (((array)$offer->attributes())['@attributes'] as $key => $value) {
                $children[$key] = $value;
            }
            if (isset($children['param'])) {
                unset($children['param']);
            }
            if (isset($children['description'])) {
                $description = (string)$offer->description;
                if (strlen($description) > 5) {
                    $children['description'] = $description;
                } else {
                    unset($children['description']);
                }
            }
            $children['vendorCode'] = $this->getVendorCode($children);

            $this->total++;
            Console::output('Total: ' . $this->total);
            //print_r($offer->children());
            if (isset($children['id'])) {

                Console::output($children['vendorCode']);

                $product = $this->findProductByVendorCode($children['vendorCode']);
                if (!$product) {
                    $this->notFound++;
                    $this->createProduct($children);
                } else {
                    $this->updateProduct($product, $children);
                }
            } else {
                $this->missingVendorCode++;
            }
            //$categoryId = (string)$children['categoryId'];
            //print_r($categoryId . PHP_EOL . $this->categories[$categoryId]['model']->title . PHP_EOL);
            //print_r($sku . PHP_EOL);
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getVendorCode($data)
    {
        return 'STIMMA-' . $data['id'] . (isset($data['group_id']) ? '-' . $data['group_id'] : '');
    }

    public function findProductByVendorCode($vendorCode)
    {
        $value = Attribute::findOne(['code' => 'sku'])->createValue();
        $value->value = $vendorCode;
        $query = Product::find();
        $value->addJoin($query, Product::tableName());
        $value->addWhere($query);

        return $query->one();
    }

    protected function createProduct($data)
    {
        /** @var Product $product */
        $product = new Product();
        $product = $this->fillProduct($product, $data);
        if ($product->save()) {
            $this->bindCategory($product, $data);
            Console::output('Created');
        } else {
            Console::output('Error:');
            print_r($product->errors);
        }
    }

    /**
     * @param Product $product
     * @param $data
     * @return mixed
     */
    protected function fillProduct($product, $data)
    {
        $identifierData = [
            'size' => '',
        ];
        /** Values */
        $valueAttributes = [
            'name' => 'name',
            'vendorCode' => 'sku',
            'picture' => 'photo',
            'material' => 'material',
            'Материал' => 'material',
            'Состав' => 'description',
            'sostav' => 'description',
            'description' => 'description',
        ];
        foreach ($valueAttributes as $foreignKey => $selfKey) {
            if (isset($data[$foreignKey])) {
                $product->$selfKey = $data[$foreignKey];
                if ($selfKey == 'sku') {
                    $identifierData['sku'] = $data[$foreignKey];
                }
            }
        }

        /** Options  */
        $optionalAttributes = [
            'vendor' => 'vendor',
            'size' => 'size',
            'Размер' => 'size',
            'color' => 'color',
            'Цвет' => 'color',
            'strana' => 'vendor_country',
        ];
        foreach ($optionalAttributes as $foreignKey => $selfKey) {
            if (isset($data[$foreignKey])) {
                $valueModel = $this->getAttributeOption($selfKey, $data[$foreignKey]);
                $product->$selfKey = $valueModel->id;
                if ($selfKey == 'size') {
                    $identifierData['size'] = $data[$foreignKey];
                }
            }
        }

        /** Prices */
        $price = floatval($data['price']);
        $product->base_price = ceil($price * 0.75);
        $product->price_wholesale = ceil($price * 0.95);
        $product->price_retail = ceil($price * 1.1);

        /** Qty */

        if ($data['available']) {
            $product->qty = 3;
        } else {
            $product->qty = 0;
        }
        $product->identifier = $this->buildIdentifier($identifierData);

        return $product;
    }

    protected function bindCategory($product, $data)
    {
        $category = $this->getCategoryModel($data['categoryId']);
        if ($category) {
            $relation = ProductHasCategory::findOne(['product_id' => $product->id]);
            if (!$relation) {
                $relation = new ProductHasCategory();
                $relation->product_id = $product->id;
            }
            if ($relation->category_id != $category->id) {
                $relation->category_id = $category->id;
                $relation->save(false);
            }
        }
    }

    /**
     * @param Product $product
     * @param $data
     */
    protected function updateProduct($product, $data)
    {
        $product = $this->fillProduct($product, $data);
        if ($product->save()) {
            $this->bindCategory($product, $data);
            Console::output('Updated');
        } else {
            Console::output('Error:');
            print_r($product->errors);
        }
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'extensions' => [
                'type' => self::OPTION_TYPE_TEXT,
            ],
        ];
    }
}