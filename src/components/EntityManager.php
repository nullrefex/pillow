<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class EntityManager extends Component
{
    /** @var string|null */
    public $entityClass = null;

    /** @var string|null */
    public $entitySearchClass = null;

    /** @var Model */
    protected $model;

    /** @var Model */
    protected $searchModel;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if ($this->entityClass === null) {
            throw new InvalidConfigException('"entityClass" must be set');
        }
        if ($this->entitySearchClass === null) {
            $this->entitySearchClass = $this->entityClass;
        }
    }

    /**
     * @param $params
     * @return DataProviderInterface
     */
    public function getDataProvider($params)
    {
        return $this->call('search', [$params]);
    }

    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function call($method, $params = [])
    {
        return call_user_func_array([$this->getSearchModel(), $method], $params);
    }

    /**
     * @return object|Model
     */
    public function getSearchModel()
    {
        if ($this->searchModel === null) {
            $this->searchModel = Yii::createObject($this->entitySearchClass);
        }
        return $this->searchModel;
    }

    /**
     * @return object|Model
     */
    public function getModel()
    {
        if ($this->model === null) {
            $this->model = $this->createModel();
        }
        return $this->model;
    }

    /**
     * @return object
     */
    public function createModel()
    {
        return Yii::createObject($this->entityClass);
    }

    /**
     * @param $id
     * @return array|null|Model
     */
    public function findById($id)
    {
        /** @var ActiveQuery $query */
        $query = $this->call('find');
        return $query->andWhere(['id' => $id])->one();
    }
}