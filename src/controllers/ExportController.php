<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\controllers;


use app\components\yml\YmlExporter;
use yii\web\Controller;

class ExportController extends Controller
{
    /**
     *
     */
    public function actionProm()
    {
        ini_set('memory_limit', '4096M');
        set_time_limit(-1);

        try {
            $exporter = new YmlExporter();
            $exporter->saveToFile = false;
            $exporter->export(\Yii::$app->request->get('ids', false));
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }

}