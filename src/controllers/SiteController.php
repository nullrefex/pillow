<?php

namespace app\controllers;

use nullref\core\interfaces\IAdminController;
use yii\web\Controller;


class SiteController extends Controller implements IAdminController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['/admin/main']);
    }

    public function actionBindProducts()
    {

    }
}
