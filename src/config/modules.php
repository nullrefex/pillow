<?php

return array_merge(require(__DIR__ . '/installed_modules.php'), [
    'core' => ['class' => nullref\core\Module::class],
    'eav' => [
        'class' => app\modules\eav\Module::class,
    ],
    'admin' => [
        'class' => app\modules\admin\Module::class,
    ],
    'product' => [
        'class' => app\modules\product\Module::class,
    ],
    'storage' => [
        'class' => app\modules\storage\Module::class,
        'components' => [
            'itemManager' => [
                'class' => \app\components\EntityManager::class,
                'entityClass' => \app\modules\product\models\Product::class,
            ],
        ],
    ],
    'documents' => [
        'class' => app\modules\documents\Module::class,
        'importers' => [
            'catalog' => [
                'class' => \app\components\importers\CatalogImporter::class,
            ],
            'prom_yml' => [
                'class' => \app\components\importers\PromYmlImporter::class,
            ],
            'stimma_yml' => [
                'class' => \app\components\importers\StimmaYmlImporter::class,
            ],
        ],
        'exporters' => [
            'total' => [
                'class' => app\components\exporters\TotalExporter::class,
            ],
        ],
    ],
    'settings' => [
        'class' => app\modules\settings\Module::class,
        'sourceLanguage' => 'en',
    ],
    'category' => [
        'class' => app\modules\category\Module::class,
        'controllerNamespace' => 'nullref\category\controllers',
        'classMap' => [
            'Category'=>\app\modules\category\models\Category::class,
        ]
    ],
]);