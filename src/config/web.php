<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');
$components = require(__DIR__ . '/components.php');

$config = [
    'id' => 'app',
    'name' => 'Pillow',
    'language' => 'uk',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => [
        'log',
        'category',
        app\modules\eav\Bootstrap::class,
        app\modules\documents\Bootstrap::class,
        app\modules\documents\Bootstrap::class,
    ],
    'controllerMap' => [
        'elfinder-backend' => [
            'class' => 'mihaildev\elfinder\Controller',
            'user' => 'user',
            'access' => ['@'],
            'roots' => [
                [
                    'path' => 'photos',
                    'name' => 'Photos',
                    'options' => [
                        'attributes' => [
                            [
                                'pattern' => '/\.(?:gitignore)$/',
                                'read' => false,
                                'write' => false,
                                'hidden' => true,
                                'locked' => false
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'components' => \yii\helpers\ArrayHelper::merge($components, [
        'formatter' => [
            'class' => 'app\components\Formatter',
        ],
        'request' => [
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
        'user' => [
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ]),
    'modules' => $modules,
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
        'panels' => [
            // 'queue' => 'zhuravljov\yii\queue\debug\Panel',
        ],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
