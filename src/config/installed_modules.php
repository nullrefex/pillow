<?php
return [
    "admin" => [
        "class" => "nullref\\fulladmin\\Module"
    ],
    "user" => [
        "class" => "nullref\\fulladmin\\modules\\user\\Module"
    ],
    "category" => [
        "class" => "nullref\\category\\Module"
    ]
];