<?php

return [
    'db' => require(__DIR__ . '/db.php'),
    'settings' => [
        'class' => pheme\settings\components\Settings::class,
    ],
    'mailer' => [
        'class' => yii\swiftmailer\Mailer::class,
        'useFileTransport' => YII_ENV_DEV,
    ],
    'i18n' => [
        'translations' => [
            '*' => ['class' => yii\i18n\PhpMessageSource::class],
            'admin' => ['class' => \nullref\core\components\i18n\PhpMessageSource::class],
            'category' => ['class' => \nullref\core\components\i18n\PhpMessageSource::class],
        ],
    ],
    'cache' => [
        'class' => yii\caching\FileCache::class,
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => YII_DEBUG ? ['error', 'warning', 'info'] : ['error', 'warning',],
            ],
        ],
    ],
    'mutex' => [
        'class' => yii\mutex\MysqlMutex::class,
    ],
    'queue' => [
        'class' => \zhuravljov\yii\queue\db\Queue::class,
        'as log' => \zhuravljov\yii\queue\LogBehavior::class,
        'db' => 'db',
        'tableName' => '{{%queue}}',
        'channel' => 'default',
    ],
];
