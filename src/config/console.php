<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');
$components = require(__DIR__ . '/components.php');

Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');

return [
    'id' => 'console-app',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => [
        'log',
        'core',
        'queue',
        'category',
        app\modules\eav\Bootstrap::class,
        app\modules\documents\Bootstrap::class,
    ],
    'controllerNamespace' => 'app\commands',
    'modules' => $modules,
    'params' => $params,
    'components' => \yii\helpers\ArrayHelper::merge($components, []),
];
