<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\traits;


use app\modules\eav\models\Attribute;
use app\modules\eav\models\attribute\Option;
use app\modules\eav\models\Value;

trait Importer
{
    protected $options = [];


    /**
     * @param $attributeCode
     * @param $value
     * @return Value
     */
    public function getAttributeOption($attributeCode, $value)
    {
        if (!isset($this->options[$attributeCode])) {
            $this->options[$attributeCode] = [];
        }
        if (isset($this->options[$attributeCode][$value])) {
            return $this->options[$attributeCode][$value];
        }
        $option = Option::find()->joinWith('attributeRecord')->andWhere([
            'value' => $value,
            'eav_attribute.code' => $attributeCode,
        ])->one();

        if ($option === null) {
            $option = new Option();
            /** @var Attribute $attribute */
            $attribute = Attribute::findOne(['code' => $attributeCode]);
            $option->attribute_id = $attribute->id;
            $option->value = $value;
            $option->save();
        }
        $this->options[$attributeCode][$value] = $option;

        return $this->options[$attributeCode][$value];
    }

    /**
     * @param $data
     * @return string
     */
    protected function buildIdentifier($data)
    {
        return trim($data['sku']) . '-' . trim($data['size']);
    }
}