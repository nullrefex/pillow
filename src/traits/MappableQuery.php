<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\traits;


use yii\helpers\ArrayHelper;

trait MappableQuery
{
    /**
     * Return array in [$index => $value] format from records of query models
     *
     * @param string $value
     * @param string $index
     * @return array
     */
    public function getMap($value = 'name', $index = 'id')
    {
        return ArrayHelper::map($this->all(), $index, $value);
    }
}